package com.klabs.firebase.token.decoder;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import com.klabs.firebase.token.decoder.model.AuthenticatedUser;
import com.klabs.firebase.token.exception.InvalidAuthorizationTokenException;

public class FirebaseTokenDecoderTest {
	 	
		private FirebaseTokenDecoder firebaseTokenDecoder = new FirebaseTokenDecoder();
	    private String validJwt = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImU1YTkxZDlmMzlmYTRkZTI1NGExZTg5ZGYwMGYwNWI3ZTI0OGI5ODUiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiTFVJWiBQQVVMTyBGUkFOWiIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9rb250YS0yNTI1MTIiLCJhdWQiOiJrb250YS0yNTI1MTIiLCJhdXRoX3RpbWUiOjE1Nzk4MDM4NDUsInVzZXJfaWQiOiJkOWJkNjU3Ny04NzBkLTQ3MGItODM4MS0xNTkzNmQzNjYzMGQiLCJzdWIiOiJkOWJkNjU3Ny04NzBkLTQ3MGItODM4MS0xNTkzNmQzNjYzMGQiLCJpYXQiOjE1Nzk4NzIxOTgsImV4cCI6MTU3OTg3NTc5OCwiZW1haWwiOiJsdWl6cGF1bG9mcmFuekBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInBob25lX251bWJlciI6Iis1NTQ5OTk5MTU2MTk5IiwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJwaG9uZSI6WyIrNTU0OTk5OTE1NjE5OSJdLCJlbWFpbCI6WyJsdWl6cGF1bG9mcmFuekBnbWFpbC5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.esQbCwow_IRAC6g7N7xXZ3HVmSUhTLiE4_aBSP5Pqwr0vPu0OivGEYEZXawGVEtTLj1jEaZUlWx0MaKBt7FVVkbBvkvOX9LfOmVHlxrw_w3UUF1HPAKonZ_tWi9FyzDhkfhC_OS7G4v_o9soJHb7QnFBiA5bK9WRTIo47Ritmkz-Ui_4qiistPpj6IeGIu9IzdRw43LOuMWPK7nqQPlNaXPoHyEki6NErnasdKVH1pNal-jSHrdikwcnIDXDKB0o_b-ATYRdDb7HXl0oeMNdPiFFFKbFhKVlQ3IG0CRE3enZOSfaNjnVQQETbbUUvfu3h0RZk7Aaa_hSB2WG5XPxZQ";
	    private String invalidJwtWithouDots = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImU1YTkxZDlmMzlmYTRkZTI1NGExZTg5ZGYwMGYwNWI3ZTI0OGI5ODUiLCJ0eXAiOiJKV1QifQeyJuYW1lIjoiTFVJWiBQQVVMTyBGUkFOWiIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9rb250YS0yNTI1MTIiLCJhdWQiOiJrb250YS0yNTI1MTIiLCJhdXRoX3RpbWUiOjE1Nzk4MDM4NDUsInVzZXJfaWQiOiJkOWJkNjU3Ny04NzBkLTQ3MGItODM4MS0xNTkzNmQzNjYzMGQiLCJzdWIiOiJkOWJkNjU3Ny04NzBkLTQ3MGItODM4MS0xNTkzNmQzNjYzMGQiLCJpYXQiOjE1Nzk4NzIxOTgsImV4cCI6MTU3OTg3NTc5OCwiZW1haWwiOiJsdWl6cGF1bG9mcmFuekBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInBob25lX251bWJlciI6Iis1NTQ5OTk5MTU2MTk5IiwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJwaG9uZSI6WyIrNTU0OTk5OTE1NjE5OSJdLCJlbWFpbCI6WyJsdWl6cGF1bG9mcmFuekBnbWFpbC5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQesQbCwow_IRAC6g7N7xXZ3HVmSUhTLiE4_aBSP5Pqwr0vPu0OivGEYEZXawGVEtTLj1jEaZUlWx0MaKBt7FVVkbBvkvOX9LfOmVHlxrw_w3UUF1HPAKonZ_tWi9FyzDhkfhC_OS7G4v_o9soJHb7QnFBiA5bK9WRTIo47Ritmkz-Ui_4qiistPpj6IeGIu9IzdRw43LOuMWPK7nqQPlNaXPoHyEki6NErnasdKVH1pNal-jSHrdikwcnIDXDKB0o_b-ATYRdDb7HXl0oeMNdPiFFFKbFhKVlQ3IG0CRE3enZOSfaNjnVQQETbbUUvfu3h0RZk7Aaa_hSB2WG5XPxZQ";
	    private String invalidPayloadJwt = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImU1YTkxZDlmMzlmYTRkZTI1NGExZTg5ZGYwMGYwNWI3ZTI0OGI5ODUiLCJ0eXAiOiJKV1QifQ.swaeyJuYW1lIjoiTFVJWiBQQVVMTyBGUkFOWiIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9rb250YS0yNTI1MTIiLCJhdWQiOiJrb250YS0yNTI1MTIiLCJhdXRoX3RpbWUiOjE1Nzk4MDM4NDUsInVzZXJfaWQiOiJkOWJkNjU3Ny04NzBkLTQ3MGItODM4MS0xNTkzNmQzNjYzMGQiLCJzdWIiOiJkOWJkNjU3Ny04NzBkLTQ3MGItODM4MS0xNTkzNmQzNjYzMGQiLCJpYXQiOjE1Nzk4NzIxOTgsImV4cCI6MTU3OTg3NTc5OCwiZW1haWwiOiJsdWl6cGF1bG9mcmFuekBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInBob25lX251bWJlciI6Iis1NTQ5OTk5MTU2MTk5IiwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJwaG9uZSI6WyIrNTU0OTk5OTE1NjE5OSJdLCJlbWFpbCI6WyJsdWl6cGF1bG9mcmFuekBnbWFpbC5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.esQbCwow_IRAC6g7N7xXZ3HVmSUhTLiE4_aBSP5Pqwr0vPu0OivGEYEZXawGVEtTLj1jEaZUlWx0MaKBt7FVVkbBvkvOX9LfOmVHlxrw_w3UUF1HPAKonZ_tWi9FyzDhkfhC_OS7G4v_o9soJHb7QnFBiA5bK9WRTIo47Ritmkz-Ui_4qiistPpj6IeGIu9IzdRw43LOuMWPK7nqQPlNaXPoHyEki6NErnasdKVH1pNal-jSHrdikwcnIDXDKB0o_b-ATYRdDb7HXl0oeMNdPiFFFKbFhKVlQ3IG0CRE3enZOSfaNjnVQQETbbUUvfu3h0RZk7Aaa_hSB2WG5XPxZQ";

	    @Test
	    public void test_correct_decode() {
	        AuthenticatedUser expect = AuthenticatedUser.builder()
	                .name("LUIZ PAULO FRANZ")
	                .email("luizpaulofranz@gmail.com")
	                .id("d9bd6577-870d-470b-8381-15936d36630d")
	                .build();

	        AuthenticatedUser result = firebaseTokenDecoder.execute(validJwt);
	        assertEquals(expect, result);
	    }

	   
	    @Test(expected = InvalidAuthorizationTokenException.class)
	    public void test_incorrect_token_payload() {
	        AuthenticatedUser expect = AuthenticatedUser.builder()
	                .name("LUIZ PAULO FRANZ")
	                .email("luizpaulofranz@gmail.com")
	                .id("d9bd6577-870d-470b-8381-15936d36630d")
	                .build();

	        firebaseTokenDecoder.execute(invalidPayloadJwt);
	    }

	   
	    @Test(expected = InvalidAuthorizationTokenException.class)
	    public void test_incorrect_token_dots() {
	        AuthenticatedUser expect = AuthenticatedUser.builder()
	                .name("LUIZ PAULO FRANZ")
	                .email("luizpaulofranz@gmail.com")
	                .id("d9bd6577-870d-470b-8381-15936d36630d")
	                .build();

	        firebaseTokenDecoder.execute(invalidJwtWithouDots);
	    }
}
