package com.klabs.firebase.token.exception;

public class InvalidAuthorizationTokenException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidAuthorizationTokenException() {
		super("O token informado é inválido.");
	}
}
