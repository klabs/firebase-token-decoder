package com.klabs.firebase.token.decoder;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.klabs.firebase.token.decoder.model.AuthenticatedUser;
import com.klabs.firebase.token.exception.InvalidAuthorizationTokenException;


public class FirebaseTokenDecoder {
	public AuthenticatedUser execute(String token) {
        try {
            String encodedPayload = token.substring(token.indexOf('.') + 1, token.lastIndexOf('.'));
            String decodedPayload = new String(Base64.getUrlDecoder().decode(encodedPayload), StandardCharsets.UTF_8);
            JsonObject json = new Gson().fromJson(decodedPayload, JsonObject.class);
            return AuthenticatedUser.builder()
                    .id(json.get("sub").toString().replace("\"", ""))
                    .name(json.get("name").toString().replace("\"", ""))
                    .email(json.get("email").toString().replace("\"", ""))
                    .build();
        } catch (Exception e) {
        	throw new InvalidAuthorizationTokenException();
        }
    }
}
