package com.klabs.firebase.token.decoder.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthenticatedUser {

	private String name;
	private String id;
	private String email;

}
